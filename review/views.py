from django.shortcuts import render, redirect
from .models import Review
from .forms import *
from django.contrib import messages
from django.contrib.messages import constants 

def index(request):
    all_review = Review.objects.all()
    return render(request, 'review.html', {'all_review': all_review})

def add(request):
    if request.method == "POST":
        form = ReviewForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, (f"Shared!"))
            redirect('/')
        else:
            messages.warning(request, ("Incorrect"))
            redirect('/')
    return render(request,'add.html')