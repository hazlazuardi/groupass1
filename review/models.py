from django.db import models
from item.models import Item

# Create your models here.
class Review(models.Model):
    item = models.ForeignKey(Item, on_delete=models.CASCADE, default=None)
    reviewer_name = models.CharField(max_length=250)
    review = models.CharField(max_length=1000)
    star = models.PositiveIntegerField(default=1)

    def __str__(self):
        return self.reviewer_name

