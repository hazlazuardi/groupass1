from django.db import models
from item.models import Item
from coupon.models import Coupon

class Transaction(models.Model):
    @property
    def get_transaction(self):
        try:
            return self.item.price_item * (100 - self.coupon.percentage) // 100
        except:
            return self.item.price_item 

    buyer_name = models.TextField(max_length=50)
    item = models.ForeignKey(Item,on_delete=models.PROTECT)
    total_transaction = get_transaction
    coupon = models.ForeignKey(Coupon,on_delete=models.PROTECT,null=True, blank=True) 
    buy_date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "{} - {} @ {}".format(self.buyer_name, self.item, self.buy_date)

    
        

    