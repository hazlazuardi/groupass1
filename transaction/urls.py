from django.urls import path
from . import views

app_name = "transaction"

urlpatterns = [
    path("history/",views.HistoryView, name="history"),
    path("checkout/",views.checkout, name="checkout"),
    path("add_transaction/",views.add_transaction, name="add_transaction")
]