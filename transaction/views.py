from django.shortcuts import render, redirect
from .models import Transaction
from item.models import Item
from coupon.models import Coupon
from django.utils import timezone

def HistoryView(request):
    context = {
        "transaction_list" : Transaction.objects.all().order_by('-buy_date'),
        'nav': [
            ['/', 'Home'],
            ['transaction/history', 'History'],

        ],

    }
    return render(request, "history.html", context)

def checkout(request):
    if request.method == "POST":
        
        name_item = request.POST["name_item"]
        price_item = request.POST["price_item"]
        stock_item = request.POST["stock_item"]
        desc_item = request.POST["desc_item"]
        
        try:
            name_coupon = request.POST["name_coupon"] if request.POST["name_coupon"] else ""
            percentage_coupon = request.POST["percentage_coupon"] if request.POST["percentage_coupon"] else 0
            coupon_list = Coupon.objects.filter(name__icontains = name_coupon)
            coupon_list = coupon_list.filter(percentage__gte = percentage_coupon)
            coupon_list = coupon_list.filter(expired__gte = timezone.now(), min_price__lte = price_item) 
        except:
            coupon_list = Coupon.objects.filter(expired__gte = timezone.now(), min_price__lte = price_item)[:10]
            name_coupon = ""
            percentage_coupon = ""

        item_bought = Item.objects.get(
            name_item = name_item,
            price_item = price_item,
            stock_item = stock_item,
            desc_item = desc_item
        )

        try:
            coupon_chosen = request.POST["coupon_chosen"].split("---")
            name_coupon_chosen = coupon_chosen[0]
            percentage_coupon_chosen = coupon_chosen[1]
            after_discount = int(item_bought.price_item * (100-float(percentage_coupon_chosen)) / 100)
        except:
            name_coupon_chosen = ""
            percentage_coupon_chosen = ""
            after_discount = None

        context = {
            "item_bought" : item_bought,
            "coupon_list" : coupon_list,
            "name_coupon" : name_coupon,
            "percentage_coupon" : percentage_coupon,
            "name_coupon_chosen" : name_coupon_chosen,
            "percentage_coupon_chosen" : percentage_coupon_chosen,
            "after_discount" : after_discount,
            'nav': [
            ['/', 'Home'],
            ['transaction/history', 'History'],

        ],

        }

        return render(request, "checkout.html", context)
    else:
        return redirect("item:index")

def add_transaction(request):
    if request.method == "POST":

        name_item = request.POST["name_item"]
        price_item = request.POST["price_item"]
        stock_item = request.POST["stock_item"]
        desc_item = request.POST["desc_item"]

        buyer_name = request.POST["first_name"] + " " + request.POST["last_name"]

        item_bought = Item.objects.get(
            name_item = name_item,
            price_item = price_item,
            stock_item = stock_item,
            desc_item = desc_item
        )

        try:
            name_coupon_chosen = request.POST["coupon_chosen_name"]
            percentage_coupon_chosen = request.POST["coupon_chosen_percentage"]
            coupon_chosen = Coupon.objects.get(name=name_coupon_chosen, percentage = percentage_coupon_chosen)
        except:
            coupon_chosen = None

        new_transaction = Transaction.objects.create(
            buyer_name = buyer_name,
            item = item_bought,
            coupon = coupon_chosen
        ).save()
        
    
        item_bought.stock_item -= 1 
        item_bought.save()

    return redirect("item:index")