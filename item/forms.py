from django import forms
# from django.utils.timezone import now
from .models import Item

class SearchForm(forms.ModelForm):
    class Meta:
        model = Item
        fields = ['name_item','category']
        labels ={'name_item': 'Looking for something?' }