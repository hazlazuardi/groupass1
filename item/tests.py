from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from django.apps import apps
from datetime import datetime
from django.utils import timezone
from .views import home
from .apps import *
from .models import UserInput
from .forms import status_input
import time

# Create your tests here.

#From story6 haz
class Home_UnitTest(TestCase):

    def test_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code,200)

    def test_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, home)

    def test_index_contains_greeting(self):
        response = Client().get('')
        response_content = response.content.decode('utf-8')
        self.assertIn("Sale", response_content)
