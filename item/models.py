from django.db import models

# Create your models here.
class Category(models.Model):
    name = models.CharField(max_length=15)
    class Meta:
        ordering = ('name',)
        verbose_name_plural = 'Categories'

    objects = models.Manager()
    def __str__(self):
        return self.name
        

class Item(models.Model):
    name_item = models.CharField(max_length=50)
    category = models.ForeignKey(Category, on_delete=models.CASCADE, default=1)
    stock_item = models.PositiveSmallIntegerField(default=0)
    price_item = models.IntegerField()
    desc_item = models.TextField(max_length=250)

    def __str__(self):
        return f'{self.name_item}'