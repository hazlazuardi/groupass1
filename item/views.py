from django.shortcuts import render
from .models import Item
from .forms import SearchForm
from django.db.models import Q
from review.forms import ReviewForm

# Create your views here.
def index(request):
    all_item = Item.objects.all()
    context={
        'all_item':all_item,
        'form': SearchForm(),
        'nav': [
            ['/', 'Home'],
            ['transaction/history', 'History'],

        ],
    }
    # form = SearchForm()
    if request.method == 'POST':
        query = request.POST['name_item']
        all_item_name = all_item.filter(name_item__icontains=query)
        all_item_category = all_item.filter(category__name__icontains=query)
        context['all_item'] = all_item_name | all_item_category
        return render(request, 'landing.html', context)
    return render(request, "landing.html",context)

def dynamicdet(request, index):
    dynamicdet_item = Item.objects.get(pk=index)
    context={
            "dynamicdet_item" : dynamicdet_item,        
            'nav': [
            ['/', 'Home'],
            ['transaction/history', 'History'],

        ],
    }
    if request.method == "POST":
        review_form = ReviewForm(request.POST)
        if review_form.is_valid():
            review = review_form.save(commit=False)
            review.item = dynamicdet_item
            review.save()

    return render(request, "item.html", context)

