from django.urls import path, include
from . import views

app_name = 'item'

urlpatterns = [
    path('', views.index, name="index"),
    path('<int:index>/', views.dynamicdet, name="dynamicdet")
]
