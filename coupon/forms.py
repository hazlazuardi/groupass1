from django import forms
from django.forms import ModelForm, Textarea
# from django.utils.timezone import now
from .models import Coupon

class CouponForm(forms.ModelForm):
    class Meta:
        model = Coupon
        fields = ['name', 'percentage']
