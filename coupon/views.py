from django.shortcuts import render
from .models import Coupon
from .forms import CouponForm
from django.db.models import Q

def index(request):
    qs = Coupon.objects.all()
    context = {
        'queryset' : qs,
        'form' : CouponForm(),
        'nav': [
            ['/', 'Home'],
            ['transaction/history', 'History'],

        ],

    }
    if request.method == 'POST':
        name = request.POST['name']
        percentage = request.POST['percentage'] if request.POST['percentage'] != '' else 0
        qs = qs.filter(name__icontains=name)
        qs = qs.filter(percentage__gte = percentage )
        context['queryset'] = qs
        return render(request, 'coupon.html', context)
    return render(request, 'coupon.html', context)
