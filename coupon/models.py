from django.db import models
from django.db.models import fields
# from .utils import generate_time
from django.utils.timezone import now
from django.core.validators import MaxValueValidator

def is_number(s):
    if s is None:
        return False
    try:
        float(s)
        return True
    except ValueError:
        return False

class PercentageField(fields.FloatField):
    def to_python(self, value):
        val = super(PercentageField, self).to_python(value)
        if is_number(val):
            return val/100
        return val

    def prepare_value(self, value):
        val = super(PercentageField, self).prepare_value(value)
        if is_number(val) and not isinstance(val, str):
            return str((float(val)*100))
        return val

class Coupon(models.Model):
    name = models.CharField(max_length=20, blank=True)
    percentage = models.PositiveSmallIntegerField(blank=True, validators=[MaxValueValidator(100)])
    expired = models.DateField()
    min_price = models.DecimalField(default=0, decimal_places=2, max_digits=10,)

    class Meta:
        ordering = ('name','percentage')
        verbose_name_plural = 'Coupons'

    def __str__(self):
        return self.name
    objects = models.Manager()